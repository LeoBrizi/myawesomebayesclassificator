from tqdm import tqdm 

class BayesianClassificator:
	def __init__(self, classes, vocabulary):
		self.vocabulary = vocabulary
		self.classes = classes
		self.probClas = {}
		self.probWordClas = {}
		self.alpha = 1
	
	def getAlpha(self):
		return self.alpha;
	def setAlpha(self,num):
		self.alpha = int(num) 

	def train_with_bernoulli(self,trainingSet):
		print("training bernoulli distribution")
		for clasJ in tqdm(self.classes):
			docsj = list(filter(lambda x:x[0]==clasJ,trainingSet))#prendo i cocumenti della classe j
			tj = len(docsj)
			self.probClas[clasJ] = tj/len(trainingSet)
			for word in tqdm(self.vocabulary):
				tij = 0
				for doc in docsj:
					if (word in doc[1]):
						tij = tij+1
				self.probWordClas[(word,clasJ)] = (tij+self.alpha)/(tj+self.alpha*2)

	def train_with_multinomial(self,trainingSet):
		print("training multinomial distribution")
		for clasJ in tqdm(self.classes):
			docsj = list(filter(lambda x:x[0]==clasJ,trainingSet))
			tj = len(docsj)
			self.probClas[clasJ] = tj/len(trainingSet)
			TFj = []
			for doc in docsj:
				TFj.extend(doc[1])
			for word in tqdm(self.vocabulary):
				TFij = TFj.count(word)
				self.probWordClas[(word,clasJ)] = (TFij +1)/(len(TFj)+self.alpha*len(self.vocabulary))	

	def evaluate_precision(self,testSet,i):
		countTP = 0;
		countFP = 0;
		for doc in testSet:
			predClas = self.classify(doc[1])
			trueClas = doc[0]
			if(predClas == trueClas and trueClas == self.classes[i]):#true positive
				countTP += 1
			elif(predClas == self.classes[i] and trueClas != self.classes[i]):
				countFP += 1
		return countTP / (countTP+countFP)

	def evaluate_recall(self,testSet,i):
		countTP = 0;
		countFN = 0;
		for doc in testSet:
			predClas = self.classify(doc[1])
			trueClas = doc[0]
			if(predClas == trueClas and trueClas == self.classes[i]):#true positive
				countTP += 1
			elif(predClas != trueClas and trueClas == self.classes[i]):
				countFN += 1
		return countTP / (countTP+countFN)

	def evaluate_accuracy(self,testSet):
		countMis = 0;
		for doc in testSet:
			predClas = self.classify(doc[1])
			if(predClas != doc[0]):
				countMis += 1
		return (len(testSet)-countMis)/len(testSet)

	#it works only with two classes
	def compute_confusion_matrix(self,testSet):
		confMat = [[]]
		for c in self.classes:
			countY = 0
			countN = 0
			for doc in testSet:
				if(doc[0] != c):
					continue
				pred = self.classify(doc[1])
				if(pred == doc[0]):
					countY += 1
				elif(pred != doc[0]):
					countN += 1
			confMat.append([c,countY,countN])
		return confMat

	def classify(self,d):
		result = {}
		for c in self.classes:
			prodT = 1
			for w in d:
				prodT = prodT * self.probWordClas[(w,c)]#.get((w,c),1)
			result[c] = self.probClas[c] * prodT
		massimo = 0;
		res = ""
		for key in result.keys():
			if(massimo <= result[key]):
				massimo = result[key]
				res = key
		return res


