from bayesClassificator import *
import random
import re

pathDataSet = "./SMSSpamCollection"
voc = []
classes = []
dataSet = []
dataSetFile = open(pathDataSet,'r')
specialC = "\"!$%&/()=?\'^.,;:-*+#"

while 1:
	line = dataSetFile.readline()
	if not line:
		break
	#for c in specialC:
	#	line = line.replace(c," ")

	line = re.sub("[^A-Za-z ]+"," ",line).lower()
	words = line.split()
	if(words[0] not in classes):
		classes.append(words[0])
	for w in words[1:]:
		#if(len(w)<=2):
		#	continue
		if(w not in voc):
			voc.append(w);
	dataSet.append((words[0],words[1:]))
dataSetFile.close()

print("classes of documents: ",classes)
print("first row of the dataset: ",dataSet[4000])


random.shuffle(dataSet)

trainingSet = dataSet[:int(len(dataSet)*0.66)]
testSet = dataSet[int(len(dataSet)*0.66):]

classificatore = BayesianClassificator(classes, voc)

print("BERNOULLI")

classificatore.train_with_bernoulli(trainingSet)
print("")

print("PRECISION: ",classificatore.evaluate_precision(testSet,1))
print("RECALL: ",classificatore.evaluate_recall(testSet,1))
print("CM: ",classificatore.compute_confusion_matrix(testSet))
print("ACCURACY: ",classificatore.evaluate_accuracy(testSet))
print(classificatore.classify(dataSet[4000][1]))

print("MULTINOMIAL")

classificatore.train_with_multinomial(trainingSet)

print("")

print("PRECISION: ",classificatore.evaluate_precision(testSet,1))
print("RECALL: ",classificatore.evaluate_recall(testSet,1))
print("CM: ",classificatore.compute_confusion_matrix(testSet))
print("ACCURACY: ",classificatore.evaluate_accuracy(testSet))
print(classificatore.classify(dataSet[4000][1]))
