from tqdm import tqdm 

class BayesianClassificator:
	def __init__(self):
		self.classes = []
		self.probClas = {}
		self.probWordClas = {}
		self.alpha = 1.0
	
	def getAlpha(self):
		return self.alpha;
	def setAlpha(self,num):
		self.alpha = num

	def train_with_bernoulli(self,trainingSet):
		print("training bernoulli distribution")
		vocabulary = {}
		occurrenceClasses = {}

		for instance in tqdm(trainingSet):
			occurrenceClasses[instance[0]] = occurrenceClasses.get(instance[0],0.0)+1.0;                     # tj for each class 
			for word in instance[1]:
				vocabulary[(instance[0],word)] = vocabulary.get((instance[0],word),0.0)+1.0;				   #tij for each word in the feature of class
		for i in occurrenceClasses.keys():
			self.probClas[i] = occurrenceClasses[i]/len(trainingSet);                                         #self.probClas[clasJ] = tj/len(trainingSet)
		for i in vocabulary.keys():
			self.probWordClas[(i[1],i[0])] = float(vocabulary[i]+self.alpha)/float(occurrenceClasses[i[0]]+self.alpha*2)

		self.classes.extend(occurrenceClasses.keys())

		print("the training of bernoulli is finished")

		''' BAD IMPLEMENTION
		for clasJ in tqdm(self.classes):
			docsj = list(filter(lambda x:x[0]==clasJ,trainingSet))#prendo i documenti della classe j
			tj = len(docsj)
			self.probClas[clasJ] = tj/len(trainingSet)
			for word in tqdm(self.vocabulary):          #tanto lento
				tij = 0
				for doc in docsj:
					if (word in doc[1]):
						tij = tij+1
				self.probWordClas[(word,clasJ)] = (tij+self.alpha)/(tj+self.alpha*2)
		'''

	def train_with_multinomial(self,trainingSet):
		print("training multinomial distribution")
		words = set()
		wordsForClasses = {}
		occurrenceClasses = {}
		vocabulary = {}

		for instance in tqdm(trainingSet):
			occurrenceClasses[instance[0]] = occurrenceClasses.get(instance[0],0.0)+1.0;                     # tj for each class 
			wordsForClasses[instance[0]] = wordsForClasses.get(instance[0],0.0)+len(instance[1])
			for word in instance[1]:
				words.add(word)
				vocabulary[(instance[0],word)] = vocabulary.get((instance[0],word),0.0)+1.0;				   #tij for each word in the feature of class

		for i in occurrenceClasses.keys():
			self.probClas[i] = occurrenceClasses[i]/len(trainingSet);                                         #self.probClas[clasJ] = tj/len(trainingSet)

		for i in vocabulary.keys():
			self.probWordClas[(i[1],i[0])] = (vocabulary[i]+1)/(wordsForClasses[i[0]]+self.alpha*len(words))
		print("the training of the multinomial is finished")

		print(vocabulary)

		print("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++")

		print(wordsForClasses)

		print("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++")

		print(words)

		print("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++")

		''' BAD IMPLEMENTATION
		for clasJ in tqdm(self.classes):
			docsj = list(filter(lambda x:x[0]==clasJ,trainingSet))
			tj = len(docsj)
			self.probClas[clasJ] = tj/len(trainingSet)
			TFj = []
			for doc in docsj:
				TFj.extend(doc[1])                              #all the word for the class
			for word in tqdm(self.vocabulary):
				TFij = TFj.count(word)
				self.probWordClas[(word,clasJ)] = (TFij +1)/(len(TFj)+self.alpha*len(self.vocabulary))	
		'''

	def evaluate_precision(self,testSet,i):
		if(i not in self.classes):
			print("there isn't any "+i+" class, let's try with an other class")
			return

		countTP = 0;
		countFP = 0;
		for doc in testSet:
			predClas = self.classify(doc[1])
			trueClas = doc[0]
			if(predClas == trueClas and trueClas == i):#true positive
				countTP += 1.0
			elif(predClas == i and trueClas != i):
				countFP += 1.0
		return float(countTP) / float(countTP+countFP)

	def evaluate_recall(self,testSet,i):
		if(i not in self.classes):
			print("there isn't any "+i+" class, let's try with an other class")
			return

		countTP = 0;
		countFN = 0;
		for doc in testSet:
			predClas = self.classify(doc[1])
			trueClas = doc[0]
			if(predClas == trueClas and trueClas == i):#true positive
				countTP += 1.0
			elif(predClas != trueClas and trueClas == i):
				countFN += 1.0
		return float(countTP) / float(countTP+countFN)

	def evaluate_accuracy(self,testSet):
		countMis = 0;
		for doc in testSet:
			predClas = self.classify(doc[1])
			if(predClas != doc[0]):
				countMis += 1.0
		return float(len(testSet)-countMis)/len(testSet)

	#it doesn't work :) don't try this
	def compute_confusion_matrix(self,testSet):
		confMat = [[]]
		for c in self.classes:
			countY = 0
			countN = 0
			for doc in testSet:
				if(doc[0] != c):
					continue
				pred = self.classify(doc[1])
				if(pred == doc[0]):
					countY += 1
				elif(pred != doc[0]):
					countN += 1
			confMat.append([c,countY,countN])
		return confMat

	def classify(self,d):
		result = {}
		for c in self.classes:
			prodT = 1.0
			for w in d:
				prodT = prodT * self.probWordClas.get((w,c),1)
			result[c] = self.probClas[c] * prodT
		massimo = 0;
		res = ""
		for key in result.keys():
			if(massimo <= result[key]):
				massimo = result[key]
				res = key
		return res


